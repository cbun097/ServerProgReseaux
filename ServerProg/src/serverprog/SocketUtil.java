package serverprog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class SocketUtil
{
    protected Socket socket;
    
    public SocketUtil(Socket socket)
    {
        this.socket = socket;
    }
    
    public void envoyer(String str)
    {
        try
        {
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.println(str);
            writer.flush();
        } 
        catch (IOException ex) 
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public String recevoir()
    {
        try 
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            return reader.readLine();
        } 
        catch (IOException ex) 
        {
            System.err.println(ex.getMessage());
        }
        
        return null;
    }
}
