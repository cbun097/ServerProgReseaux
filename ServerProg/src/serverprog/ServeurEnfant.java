package serverprog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.Socket;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class ServeurEnfant extends SocketUtil implements Runnable
{
    GestionnaireFilmsRandom gest;
    
    public ServeurEnfant(Socket socket, GestionnaireFilmsRandom gest)
    {
        super(socket);
        this.gest = gest;
    }

    @Override
    public void run() 
    {
        String line;
        Gson gson = new GsonBuilder().create(); 
        while((line = recevoir()) != null)
        {
            String[] splits = line.split("::");
            String demande = splits[0];

            switch(demande)
            {
                // Inscription string => "inscription:user=?;nom=?;prenom=?;password=?;email=?;pays=?"
                case "inscription" :
                    System.out.format("Reception de la demande d'incription du client %s:%s\n", socket.getInetAddress().getHostAddress(), socket.getPort());
                    String user = splits[1].split(";")[0].split("=")[1];
                    String nom = splits[1].split(";")[1].split("=")[1];
                    String prenom = splits[1].split(";")[2].split("=")[1];
                    String pass = splits[1].split(";")[3].split("=")[1];
                    String email = splits[1].split(";")[4].split("=")[1];
                    String pays = splits[1].split(";")[5].split("=")[1];
                    if(Database.getInstance().insertUsers(user, nom, prenom, pass, email, pays))
                        envoyer("inscription::true");
                    else
                        envoyer("inscription::false");
                    break;
                // Connection string => "connect:user=?;password=?"
                case "connect" :
                    String username = splits[1].split(";")[0].split("=")[1];
                    String password = splits[1].split(";")[1].split("=")[1];
                    String reponse = "";
                    if(Database.getInstance().checkUserCredentials(username, password))
                        reponse = "connect::true";
                    else
                        reponse = "connect::false";
                    envoyer(reponse);
                    break;
                // Envoyer la suggestion des films (5 films randoms)
                case "suggestion" :
                    if(splits[1].equals("ask"))
                    {
                        String jsonObj = gson.toJson(this.gest.getRandomFilms());
                        envoyer("suggestion::" + jsonObj);
                        //envoyerObject(gest.getRandomFilms());
                        System.out.println("Random list:" + jsonObj.length());
                    }
                    break;
                // Enregistrer une liste et ajouter un film
                case "savefilm":            
                    String nomListe = splits[1].split(";")[0].split("=")[1];
                    String usernameliste = splits[1].split(";")[1].split("=")[1];
                    String filmid = splits[1].split(";")[2].split("=")[1];
                    if(Database.getInstance().insertListe(nomListe, usernameliste, filmid))
                    {
                        reponse = "listeSave::true";
                        System.out.println("true");
                    }
                    else
                        reponse = "listeSave::false";
                    envoyer(reponse);
                    break;
                // Envoyer la liste au client
                case "envoieListe" :
                    System.out.println(splits[1]);
                    String userListEnvoi = splits[1].split(";")[0].split("=")[1];
                    String listeItems = splits[1].split(";")[1].split("=")[1];
                    System.out.println("voici la liste: " + listeItems);
                    System.out.println("envoie la liste");
                    String listJson = gson.toJson(Database.getInstance().getSavedList(userListEnvoi, listeItems));
                    System.out.println("envoie " + listJson.length());
                    System.out.println("envoie " + listJson.toString() );
                    envoyer("envoieListe::" + listJson);
                    break;
                // Envoyer le nom des listes d'un utilisateur
                case "allListe" :
                    String userListe = splits[1].split("=")[1];
                    String jsonList = gson.toJson(Database.getInstance().getListesUtilisateurs(userListe));
                    envoyer("allListe::" + jsonList);
                    break;
            }
        }
    }
}
