package serverprog;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class ServeurPrincipal extends Thread
{
    
    private int port;
    private String ip;
    
    public ServeurPrincipal(String ip, int port)
    {
        this.ip = ip;
        this.port = port;
    }
    
    @Override
    public void run()
    {
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(Integer.MAX_VALUE, Integer.MAX_VALUE, 10, TimeUnit.SECONDS, new ArrayBlockingQueue(2), threadFactory, 
            new RejectedExecutionHandler() 
            {
                @Override
                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) 
                {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            }
        );
        try 
        {
            ServerSocket socket = new ServerSocket();
            socket.bind(new InetSocketAddress(ip, port));
            socket.setReuseAddress(true);
            GestionnaireFilmsRandom gestionnaireFilmsRandom = new GestionnaireFilmsRandom();
   
            while(true)
            {
                try
                {
                    System.out.println(String.format("Je suis en attente de connexion sur le port %d", socket.getLocalPort()));
                    Socket socketComm = socket.accept();
                    executorPool.execute(new ServeurEnfant(socketComm, gestionnaireFilmsRandom));
                    System.out.println(String.format("Le client %s:%s à été redirigé vers un thread enfant", socketComm.getInetAddress().getHostAddress(), socketComm.getPort()));
                }
                catch(IOException ex)
                {
                    System.err.println(ex.getMessage());
                }
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ServeurPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
