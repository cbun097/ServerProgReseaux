package serverprog;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class ListeSave implements Serializable{
    //liste de sauvegarde des films fait par l'utilisateur
    
    private String nomListe, username;
    
    private ArrayList<Films> itemListe;
    
    //Constructeur
    public ListeSave(String nomListe, String username, ArrayList<Films> itemListe) 
    {
        this.nomListe = nomListe;
        this.username = username;
        this.itemListe = itemListe;
    }

    //Methode d'access
    public String getNomListe() {
        return nomListe;
    }

    public void setNomListe(String nomListe) {
        this.nomListe = nomListe;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username) 
    {
        this.username = username;
    }

    public ArrayList<Films> getItemListe() 
    {
        return itemListe;
    }

    public void setItemListe(ArrayList<Films> itemListe) 
    {
        this.itemListe = itemListe;
    }

    //Methode toString
    @Override
    public String toString() 
    {
        return "ListeSave{" + "nomListe=" + this.nomListe + ", username=" + this.username + '}';
    }
}
