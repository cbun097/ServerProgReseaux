package serverprog;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class Database 
{
    //propriete url
    private Connection cn;
    //etablir une connexion vers la base de donnees
    private String url;
    
    //creer la DB    
    //Constructeur avec la chaine de connexion
    private Database(String url)
    {
        this.url = url;
        try 
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } 
        catch (ClassNotFoundException ex) 
        {
            System.err.println(ex.getMessage());
        }
    }
    
    // SINGLETON
    private static Database instance;
    public static Database getInstance()
    {
        if(instance == null)
            instance = new Database("jdbc:sqlserver://localhost;databaseName=MovieJar;username=sa;password=sql");
        
        return instance;
    }
    
    //Verifier si l'utilisateur qui entre dans le login existe ou pas
    public boolean checkUserCredentials(String username, String password)
    {
        try 
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Users WHERE username = ? AND PasswordUser = ?");
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            return set.next();
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try 
            {
                if(cn != null)
                    cn.close();
            } 
            catch (SQLException ex) 
            {
                System.err.println(ex.getMessage());
            }
        }
        
        return false;
    }
    
    // Obtenir la liste de tous les films
    public ArrayList<Films> getListeTotalFilm()
    {
        try 
        {
            ArrayList<Films> liste = new ArrayList<>();
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Films");
            ResultSet rs = statement.executeQuery();
            while(rs.next())
            {
                String idFilm = rs.getString("NumFilm");
                String nomFilm = rs.getString("NomFilm");
                double dureeFilm = rs.getDouble("Duree");
                String nomDirecteur = rs.getString("NomDirecteur");
                String acteurs = rs.getString("ActeursActrice");
                int rating = rs.getInt("Rating");
                String pays = rs.getString("PaysOrigine");
                String langues = rs.getString("LanguesParles");
                String dateSortie = rs.getString("DateSortie");
                String imageUrl = rs.getString("ImageFilm");
                String resume = rs.getString("ResumeFilme");
                
                Films film = new Films(idFilm, nomFilm, nomDirecteur, pays, langues, dureeFilm, rating, dateSortie, imageUrl, resume);
                ArrayList<String> listeActeurs = new ArrayList<>(Arrays.asList(acteurs.replace(" ", "").split(",")));
                film.setListeActeurs(listeActeurs);
                liste.add(film);
            }
            return liste;
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try 
            {
                if(cn != null)
                    cn.close();
            } 
            catch (SQLException ex) 
            {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }
    
    // Insérer un utilisateur dans la BD
    public boolean insertUsers(String user, String nom, String prenom, String pass, String email, String pays)
    {
        try
        {
            cn = DriverManager.getConnection(url);
            //Query pour insert les nouveaux utilisateurs dans la DB
            String query = "INSERT INTO Users(username,Nom,Prenom,PasswordUser,email,pays) VALUES (?,?,?,?,?,?)";
            PreparedStatement requete = cn.prepareStatement(query);
            requete.setString(1, user);
            requete.setString(2, nom);
            requete.setString(3, prenom);
            requete.setString(4, pass);
            requete.setString(5, email);
            requete.setString(6, pays);
            //executer la requete
            return requete.executeUpdate() > 0;
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }  
        finally
        {
            try 
            {
                //fermer la connexion
                cn.close();
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return false;
    }
    
    // Inserer un film dans une liste de film dans la BD
    public boolean insertListe(String nomListe, String username, String filmID)
    {
        boolean result = false;
        try 
        {
            //query pour insert les listes dans la DB
            PreparedStatement requete;
            // Créer la liste si elle n'existe pas
            if(!checkListExist(nomListe))
            {
                cn = DriverManager.getConnection(url);
                requete = cn.prepareStatement("INSERT INTO ListeFilmUtilisateur(NomListe,username) VALUES (?,?)");
                requete.setString(1, nomListe);
                requete.setString(2, username);
                result = requete.executeUpdate() > 0;
            }
            
            // Ajouter le film dans la liste
            cn = DriverManager.getConnection(url);
            requete = cn.prepareStatement("INSERT INTO ItemListeFilm(NomListe,NumFilm) VALUES (?,?)");
            requete.setString(1, nomListe);
            requete.setString(2, filmID);
            result = requete.executeUpdate() > 0;
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
            result = false;
        }
        finally
        {
            try 
            {
                //fermer la connexion
                cn.close();
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return result;
    }
    
    // Obtenir les films d'un liste d'un utilisateur
    public ListeSave getSavedList(String username, String nomListe)
    {
        try 
        {
            //query pour insert les listes dans la DB
            if(checkListExist(nomListe))
            {
                cn = DriverManager.getConnection(url);
                String query =  "SELECT NumFilm FROM ItemListeFilm " +
                                "INNER JOIN ListeFilmUtilisateur ON ListeFilmUtilisateur.NomListe = ItemListeFilm.NomListe " +
                                "WHERE ListeFilmUtilisateur.username = ? AND ItemListeFilm.NomListe = ?";
                PreparedStatement requete = cn.prepareStatement(query);
                requete.setString(1, username);
                requete.setString(2, nomListe);
                ResultSet rs = requete.executeQuery();
                ArrayList<Films> numFilms = new ArrayList<>();
                while(rs.next())
                {
                    String id = rs.getString("NumFilm");
                    numFilms.add(ServerProg.listeTotalFilm.stream().filter(film -> film.getIdFilm().equals(id)).findFirst().get());
                }
                
                return new ListeSave(nomListe, username, numFilms);
            }
            else
            {
                return null;
            }
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try 
            {
                //fermer la connexion
                cn.close();
            } 
            catch (SQLException ex) 
            {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }
    
    // Verifier si la liste existe
    public boolean checkListExist(String nom)
    {
        try 
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM ListeFilmUtilisateur WHERE NomListe = ?");
            statement.setString(1, nom);
            ResultSet set = statement.executeQuery();
            return set.next();
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try 
            {
                if(cn != null)
                    cn.close();
            } 
            catch (SQLException ex) 
            {
                System.err.println(ex.getMessage());
            }
        }
        
        return false;
    }
    
    // Obtenir toutes les listes (leur nom) d'un utilisateur
    public ArrayList<String> getListesUtilisateurs(String username)
    {
        try 
        {
            ArrayList<String> list = new ArrayList<>();
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT NomListe FROM ListeFilmUtilisateur WHERE username = ?");
            statement.setString(1, username);
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                list.add(set.getString("NomListe"));
            }
            
            return list;
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try 
            {
                if(cn != null)
                    cn.close();
            } 
            catch (SQLException ex) 
            {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }
}
