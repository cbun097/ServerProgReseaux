package serverprog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class Films implements Serializable
{
    //Proprietes
    private String IdFilm, nomFilm, nomDirecteur, paysOrigine, languesOrigine, imageUrl, resume, dateSorite;
    private double duree;
    private int rating;
    private List<String> listeActeurs;
    
    //Constructeur par default 
    public Films(){}
    
    //Constructeur par parametre
    public Films(String IdFilm, String nomFilm, String nomDirecteur, String paysOrigine, String languesOrigine, double duree, int rating, String dateSorite, String imageUrl, String resume) 
    {
        this.IdFilm = IdFilm;
        this.nomFilm = nomFilm;
        this.nomDirecteur = nomDirecteur;
        this.paysOrigine = paysOrigine;
        this.languesOrigine = languesOrigine;
        this.duree = duree;
        this.rating = rating;
        this.dateSorite = dateSorite;
        this.imageUrl = imageUrl;
        this.resume = resume;
        //initialiser la liste
        listeActeurs = new ArrayList<>();
    }
    
    //Methode d'access
    public String getIdFilm() {       
        return IdFilm;
    }

    public void setIdFilm(String IdFilm) {
        this.IdFilm = IdFilm;
    }

    public String getNomFilm() {
        return nomFilm;
    }

    public void setNomFilm(String nomFilm) {
        this.nomFilm = nomFilm;
    }

    public String getNomDirecteur() {
        return nomDirecteur;
    }

    public void setNomDirecteur(String nomDirecteur) {
        this.nomDirecteur = nomDirecteur;
    }

    public String getPaysOrigine() {
        return paysOrigine;
    }

    public void setPaysOrigine(String paysOrigine) {
        this.paysOrigine = paysOrigine;
    }

    public String getLanguesOrigine() {
        return languesOrigine;
    }

    public void setLanguesOrigine(String languesOrigine) {
        this.languesOrigine = languesOrigine;
    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDateSorite() {
        return dateSorite;
    }

    public void setDateSorite(String dateSorite) {
        this.dateSorite = dateSorite;
    }

    public List<String> getListeActeurs() {
        return listeActeurs;
    }

    public void setListeActeurs(List<String> listeActeurs) {
        this.listeActeurs = listeActeurs;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    @Override
    public String toString() {
        return "Films{" + "IdFilm=" + IdFilm + ", nomFilm=" + nomFilm + ", nomDirecteur=" + nomDirecteur + ", paysOrigine=" + paysOrigine + ", languesOrigine=" + languesOrigine + ", duree=" + duree + ", rating=" + rating + ", dateSorite=" + dateSorite + ", listeActeurs=" + listeActeurs + '}';
    }    
}
