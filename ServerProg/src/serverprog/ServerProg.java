package serverprog;

import java.util.ArrayList;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class ServerProg 
{
    public static ArrayList<Films> listeTotalFilm = new ArrayList<>();
    public static void main(String[] args) 
    {
        String ip = "10.4.129.22";
        int port = 2011;
        
        listeTotalFilm = Database.getInstance().getListeTotalFilm();
        
        new ServeurPrincipal(ip, port).start();
    }
}
