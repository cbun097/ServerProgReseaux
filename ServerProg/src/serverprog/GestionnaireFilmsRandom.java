package serverprog;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Claire Bun, Anthony Whelan
 */
public class GestionnaireFilmsRandom
{
    private ArrayList<Films> listeFilmsRandom;
    
    public GestionnaireFilmsRandom()
    {
        listeFilmsRandom = new ArrayList<>();
    }
    
    public ArrayList<Films> getRandomFilms()
    {
        // Pour empecher un autre utilisateur de reset la liste des films Randoms
        synchronized(this)
        {
            listeFilmsRandom.clear();
            Random rand = new Random();
            for(int i = 0; i < 5; i++)
                listeFilmsRandom.add(ServerProg.listeTotalFilm.get(rand.nextInt(ServerProg.listeTotalFilm.size())));
            
            return listeFilmsRandom;
        }
    }
}
